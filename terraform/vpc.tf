resource "aws_vpc" "main_vpc" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_hostnames = true
    tags = {
        Name = "terraform-aws-vpc"
    }
}

resource "aws_eip" "ansible_master" {
    instance = "${aws_instance.ansible_master.id}"
    vpc = true
}

resource "aws_eip" "web" {
    instance = "${aws_instance.web.id}"
    vpc = true
}


resource "aws_route_table" "eu-central-1a-public" {
    vpc_id = "${aws_vpc.main_vpc.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.ig-main.id}"
    }

    tags = {
        Name = "Public Subnet"
    }
}

resource "aws_route_table_association" "eu-central-1a-public" {
    subnet_id = "${aws_subnet.eu-central-1a-public.id}"
    route_table_id = "${aws_route_table.eu-central-1a-public.id}"
}


resource "aws_subnet" "eu-central-1a-public" {
    vpc_id = "${aws_vpc.main_vpc.id}"

    cidr_block = "${var.public_subnet_cidr}"
    availability_zone = "eu-central-1a"

    tags = {
        Name = "Public Subnet"
    }
}


resource "aws_internet_gateway" "ig-main" {
    vpc_id = "${aws_vpc.main_vpc.id}"
}
