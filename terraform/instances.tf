resource "aws_instance" "ansible_master" {
    ami = "ami-0c960b947cbb2dd16"
    availability_zone = "eu-central-1a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.ansible_master.id}"]
    subnet_id = "${aws_subnet.eu-central-1a-public.id}"
    associate_public_ip_address = true
    source_dest_check = false


    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("aws.pem")}"
      host        = "${self.public_ip}"
    }

    #${path.module}
    provisioner "file" {
    source      = "/home/administrator/terraform/aws.pem" #"${file("aws.pem")}"
    destination = "/home/ubuntu/aws.pem"
    }

    provisioner "file" {
    source      = "/home/administrator/terraform/edited_ansible.cfg"
    destination = "/home/ubuntu/edited_ansible.cfg"
    }

    provisioner "file" {
    source      = "/home/administrator/terraform/hosts"
    destination = "/home/ubuntu/hosts"
    }


    # or create AMI with ansible installed/configured?
    provisioner "remote-exec" {
      inline = [
        "sudo apt-add-repository --yes ppa:ansible/ansible",
        "sudo apt update",
        # upgrade?
        "sudo apt-get install ansible -y",
        "sleep 1",
        "sudo chmod 400 aws.pem",
        "sudo mv /home/ubuntu/edited_ansible.cfg /etc/ansible/ansible.cfg",
        # "sudo echo ${aws_instance.web.private_ip} >> /home/ubuntu/test",
        "sudo sed -i 's/<web.private_ip>/${aws_instance.web.private_ip}/' hosts",
        "sudo sed -i 's/<db.private_ip>/${aws_instance.db.private_ip}/' hosts",
        "sudo git clone https://gitlab.com/freefakesmile/test-task.git",
        # "ansible-playbook test-task/ansible/test-task.yml",
        "sleep 1",
        # "ansible-playbook /tmp/ans_ws/site.yaml"
      ]
    }


    tags = {
        Name = "Ansible Master"
    }
}

resource "aws_instance" "web" {
    ami = "${lookup(var.amis, var.aws_region)}"
    availability_zone = "eu-central-1a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.web.id}"]
    subnet_id = "${aws_subnet.eu-central-1a-public.id}"
    associate_public_ip_address = true
    source_dest_check = false



    tags = {
        Name = "Web Server (nginx+php-fpm7.4)"
    }
}

resource "aws_instance" "db" {
    ami = "${lookup(var.amis, var.aws_region)}"
    availability_zone = "eu-central-1a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.db.id}"]
    subnet_id = "${aws_subnet.eu-central-1a-public.id}"
    associate_public_ip_address = true
    source_dest_check = false

    tags = {
        Name = "DB Server (mysql 8)"
    }
}
